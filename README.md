# WORDLE API

## Prueba técnica - DD3


Base URL: 
 [http://localhost:3000/v1](http://localhost:3000/v1)

## Config

Crear el archivos .env:

```bash
POSTGRES_HOST="localhost"
POSTGRES_PORT="5432"
POSTGRES_NAME="dd3db"
POSTGRES_USER="username"
POSTGRES_PASSWORD="password"
JWT_SECRET="secret"
DATABASE_URL="postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@${POSTGRES_HOST}:${POSTGRES_PORT}/${POSTGRES_NAME}?schema=public"

```

## Correr localmente

Levantar la base de datos

```bash
docker-compose up
```

Migrar la base de datos con Prisma

```bash
npm run migrate
```

Levantar el backend
```bash
npm run start:dev
```

Ejecutar pruebas unitarias

```bash
npm run test
```

## Endpoints 🚀

## WordFiles 

_Poblar la base de datos con palabras_

http://localhost:3000/v1/wordfile/generate

![wordfile-generate](/images/wordfile-generate.PNG)

## Auth

_Registrar un nuevo usuario_

http://localhost:3000/v1/auth/register

![auth-register](/images/auth-register.PNG)

_Login_

http://localhost:3000/v1/auth/login

![auth-login](/images/auth-login.PNG)

## Wordle

_Esto iniciará el juego creando "game" que hace referencia al contexto donde jugará muchas o una partida empezando por "play"_

http://localhost:3000/v1/wordle/create-game

![create-game](/images/create-game.PNG)

_Obtener la siguiente palabra(no repetida) que hace referencia a crear una nueva partida según el gameId_

http://localhost:3000/v1/wordle/next-word/gameId

![next-word](/images/next-word.PNG)

_Intentar adivinar la palabra_

http://localhost:3000/v1/wordle/attempt

![attempt](/images/attempt.PNG)

_Palabras más adivinadas_

http://localhost:3000/v1/wordle/most-successful-words

![most-successful-words](/images/most-successful-words.PNG)

## User

_Partidas jugadas y ganadas por un usuario_

http://localhost:3000/v1/user/statistics/userId

![statistics](/images/statistics.PNG)


_Top 10 jugadores_

http://localhost:3000/v1/user/top10

![top10](/images/top10.PNG)



## Tecnologías usadas
- Nest.js
- Docker
- Docker Compose
- PostgreSQL
- Prisma
- Typescript
- Jest


