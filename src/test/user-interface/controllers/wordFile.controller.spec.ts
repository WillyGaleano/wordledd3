import { Test } from '@nestjs/testing';
import { WordFileController } from '../../../microservice/user-interface/controllers/wordFile.controller';
import { GenerateWordsInteractor } from '../../../microservice/application-core/wordle/use-cases/generateWords.interactor';
import { ResponseDto } from '../../../microservice/application-core/wordle/dtos/index';

describe('WordFileController', () => {
  let wordFileController: WordFileController;
  let generateWordsInteractor: GenerateWordsInteractor;
  const result = {
    count: 12626,
  };
  beforeAll(async () => {
    class MockModel {
      static execute = async () => new ResponseDto(result);
    }

    const moduleRef = await Test.createTestingModule({
      controllers: [WordFileController],
      providers: [
        {
          provide: GenerateWordsInteractor,
          useValue: MockModel,
        },
      ],
    }).compile();

    generateWordsInteractor = moduleRef.get<GenerateWordsInteractor>(
      GenerateWordsInteractor,
    );
    wordFileController = moduleRef.get<WordFileController>(WordFileController);
  });

  describe('generatWordFiles', () => {
    it('should return an array of words', async () => {
      expect(await wordFileController.generateWord()).toStrictEqual(
        new ResponseDto(result),
      );
    });
  });
});
