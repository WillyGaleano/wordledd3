import { UserController } from '../../../microservice/user-interface/controllers/user.controller';
import { StatisticsInteractor } from '../../../microservice/application-core/user/use-cases/statistics.interactor';
import { Top10Interactor } from '../../../microservice/application-core/user/use-cases/top10.interactor';
import { Test, TestingModule } from '@nestjs/testing';
import { BadRequestException } from '@nestjs/common';
import { ResponseDto } from '../../../microservice/application-core/wordle/dtos/index';

describe('UserController', () => {
  let controller: UserController;
  let statisticsInteractor: StatisticsInteractor;
  let top10Interactor: Top10Interactor;
  const expectedResponse = new ResponseDto({
    userId: '123',
    name: 'John',
    lastName: 'Doe',
    cantPlays: 10,
    cantWins: 5,
  });
  class MockModel {
    static execute = async () => new ResponseDto(expectedResponse);
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        { provide: StatisticsInteractor, useValue: MockModel },
        { provide: Top10Interactor, useValue: MockModel },
      ],
    }).compile();

    controller = module.get<UserController>(UserController);
    statisticsInteractor =
      module.get<StatisticsInteractor>(StatisticsInteractor);
    top10Interactor = module.get<Top10Interactor>(Top10Interactor);
  });

  describe('statistics', () => {
    it('should return statistics for a given user', async () => {
      const userId = '123';
      const expectedResponse = new ResponseDto({
        userId,
        name: 'John',
        lastName: 'Doe',
        cantPlays: 10,
        cantWins: 5,
      });
      jest
        .spyOn(statisticsInteractor, 'execute')
        .mockImplementation(async () => expectedResponse);

      const result = await controller.statistics(userId);

      expect(statisticsInteractor.execute).toHaveBeenCalledWith(userId);
      expect(result).toBe(expectedResponse);
    });

    it('should throw BadRequestException if statisticsInteractor returns an error', async () => {
      const userId = '123';
      const errorResponse = { error: 'Invalid user ID' };
      jest
        .spyOn(statisticsInteractor, 'execute')
        .mockResolvedValueOnce(errorResponse);

      await expect(controller.statistics(userId)).rejects.toThrow(
        BadRequestException,
      );
    });
  });

  describe('top10', () => {
    it('should return top 10 records', async () => {
      const expectedResponse = {};
      jest
        .spyOn(top10Interactor, 'execute')
        .mockResolvedValueOnce(expectedResponse);

      const result = await controller.top10();

      expect(top10Interactor.execute).toHaveBeenCalled();
      expect(result).toBe(expectedResponse);
    });

    it('should throw BadRequestException if top10Interactor returns an error', async () => {
      const errorResponse = { error: 'Internal server error' };
      jest
        .spyOn(top10Interactor, 'execute')
        .mockResolvedValueOnce(errorResponse);

      await expect(controller.top10()).rejects.toThrow(BadRequestException);
    });
  });
});
