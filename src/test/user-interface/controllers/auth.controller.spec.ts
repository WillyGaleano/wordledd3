import { Test, TestingModule } from '@nestjs/testing';
import { ResponseDto } from '../../../microservice/application-core/wordle/dtos/index';
import { BadRequestException } from '@nestjs/common';
import { AuthController } from '../../../microservice/user-interface/controllers/auth.controller';
import { AuthInteractor } from '../../../microservice/application-core/auth/use-cases/auth.interactor';

describe('AuthController', () => {
  let controller: AuthController;
  let authInteractor: AuthInteractor;

  class MockModel {
    static register = async () => new ResponseDto({});
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [{ provide: AuthInteractor, useValue: MockModel }],
    }).compile();

    controller = module.get<AuthController>(AuthController);
    authInteractor = module.get<AuthInteractor>(AuthInteractor);
  });

  describe('register', () => {
    it('should call register and return the response', async () => {
      const response = new ResponseDto({});
      jest.spyOn(authInteractor, 'register').mockResolvedValue(response);
      expect(await controller.register(null)).toBe(response);
    });

    it('should throw BadRequestException if registerInteractor returns an error', async () => {
      const errorResponse = { error: 'error message' };
      jest.spyOn(authInteractor, 'register').mockResolvedValue(errorResponse);
      await expect(controller.register(null)).rejects.toThrowError(
        BadRequestException,
      );
    });
  });
});
