import { WordleController } from '../../../microservice/user-interface/controllers/wordle.controller';
import { CreateGameInteractor } from '../../../microservice/application-core/wordle/use-cases/createGame.interactor';
import { GenerateNextWordInteractor } from '../../../microservice/application-core/wordle/use-cases/generateNextWord.interactor';
import { AttemptsWordInteractor } from '../../../microservice/application-core/wordle/use-cases/attemptsWord.interactor';
import { MostSuccessfulWordsInteractor } from '../../../microservice/application-core/wordle/use-cases/mostSuccessfulWords.interactor';
import { Test, TestingModule } from '@nestjs/testing';
import {
  GameDto,
  ResponseDto,
} from '../../../microservice/application-core/wordle/dtos/index';
import { BadRequestException } from '@nestjs/common';

describe('WordleController', () => {
  let controller: WordleController;
  let createGameInteractor: CreateGameInteractor;
  let generateNextWordInteractor: GenerateNextWordInteractor;
  let attemptsWordInteractor: AttemptsWordInteractor;
  let mostSuccessfulWordsInteractor: MostSuccessfulWordsInteractor;

  class MockModel {
    static execute = async () => new ResponseDto({});
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [WordleController],
      providers: [
        { provide: CreateGameInteractor, useValue: MockModel },
        { provide: GenerateNextWordInteractor, useValue: MockModel },
        { provide: AttemptsWordInteractor, useValue: MockModel },
        { provide: MostSuccessfulWordsInteractor, useValue: MockModel },
      ],
    }).compile();

    controller = module.get<WordleController>(WordleController);
    createGameInteractor =
      module.get<CreateGameInteractor>(CreateGameInteractor);
    generateNextWordInteractor = module.get<GenerateNextWordInteractor>(
      GenerateNextWordInteractor,
    );
    attemptsWordInteractor = module.get<AttemptsWordInteractor>(
      AttemptsWordInteractor,
    );
    mostSuccessfulWordsInteractor = module.get<MostSuccessfulWordsInteractor>(
      MostSuccessfulWordsInteractor,
    );
  });

  describe('createGame', () => {
    it('should call createGameInteractor and return the response', async () => {
      const game: GameDto = {
        userId: '123',
      };
      const response = new ResponseDto({
        game: {},
        play: {},
      });
      jest.spyOn(createGameInteractor, 'execute').mockResolvedValue(response);
      expect(await controller.createGame(game)).toBe(response);
      expect(createGameInteractor.execute).toHaveBeenCalledWith(game);
    });

    it('should throw BadRequestException if createGameInteractor returns an error', async () => {
      const game: GameDto = {
        userId: '123',
      };
      const errorResponse = { error: 'error message' };
      jest
        .spyOn(createGameInteractor, 'execute')
        .mockResolvedValue(errorResponse);
      await expect(controller.createGame(game)).rejects.toThrowError(
        BadRequestException,
      );
    });
  });

  describe('nextWord', () => {
    it('should call generateNextWordInteractor and return the response', async () => {
      const gameId = '123';
      const response = new ResponseDto({});
      jest
        .spyOn(generateNextWordInteractor, 'execute')
        .mockResolvedValue(response);
      expect(await controller.nextWord(gameId)).toBe(response);
      expect(generateNextWordInteractor.execute).toHaveBeenCalledWith(gameId);
    });

    it('should throw BadRequestException if generateNextWordInteractor returns an error', async () => {
      const gameId = '123';
      const errorResponse = { error: 'error message' };
      jest
        .spyOn(generateNextWordInteractor, 'execute')
        .mockResolvedValue(errorResponse);
      await expect(controller.nextWord(gameId)).rejects.toThrowError(
        BadRequestException,
      );
    });
  });
});
