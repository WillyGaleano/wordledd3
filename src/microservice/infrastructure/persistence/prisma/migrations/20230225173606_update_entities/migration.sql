/*
  Warnings:

  - You are about to drop the column `wordTemp` on the `attempts` table. All the data in the column will be lost.
  - You are about to drop the column `successful` on the `plays` table. All the data in the column will be lost.
  - Added the required column `userWord` to the `attempts` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "attempts" DROP COLUMN "wordTemp",
ADD COLUMN     "userWord" VARCHAR(5) NOT NULL;

-- AlterTable
ALTER TABLE "plays" DROP COLUMN "successful";

-- AlterTable
ALTER TABLE "users" ALTER COLUMN "email" SET DATA TYPE VARCHAR(50),
ALTER COLUMN "password" SET DATA TYPE VARCHAR(150);
