-- CreateTable
CREATE TABLE "word_files" (
    "id" TEXT NOT NULL,
    "name" VARCHAR(5) NOT NULL,
    "createdAt" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updatedAt" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "word_files_pkey" PRIMARY KEY ("id")
);
