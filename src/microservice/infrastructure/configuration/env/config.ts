export const EnvConfiguration = () => ({
  POSTGRES_HOST: process.env.POSTGRES_HOST,
  POSTGRES_PORT: process.env.POSTGRES_PORT,
  POSTGRES_NAME: process.env.POSTGRES_NAME,
  POSTGRES_USER: process.env.POSTGRES_USER,
  POSTGRES_PASSWORD: process.env.POSTGRES_PASSWORD,
  JWT_SECRET: process.env.JWT_SECRET,
});
