import * as Joi from 'joi';

export const JoiValidationEnv = Joi.object({
  POSTGRES_HOST: Joi.required(),
  POSTGRES_PORT: Joi.required(),
  POSTGRES_NAME: Joi.required(),
  POSTGRES_USER: Joi.required(),
  POSTGRES_PASSWORD: Joi.required(),
  JWT_SECRET: Joi.required(),
});
