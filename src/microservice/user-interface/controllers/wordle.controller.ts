import { CreateGameInteractor } from '../../application-core/wordle/use-cases/createGame.interactor';
import { GenerateNextWordInteractor } from '../../application-core/wordle/use-cases/generateNextWord.interactor';
import { AttemptsWordInteractor } from '../../application-core/wordle/use-cases/attemptsWord.interactor';
import { MostSuccessfulWordsInteractor } from '../../application-core/wordle/use-cases/mostSuccessfulWords.interactor';
import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';

import {
  AttemptRequestDto,
  GameDto,
} from '../../application-core/wordle/dtos/index';
import { AuthGuard } from '@nestjs/passport';

@Controller('wordle')
export class WordleController {
  constructor(
    private readonly createGameInteractor: CreateGameInteractor,
    private readonly generateNextWordInteractor: GenerateNextWordInteractor,
    private readonly attemptsWordInteractor: AttemptsWordInteractor,
    private readonly mostSuccessfulWordsInteractor: MostSuccessfulWordsInteractor,
  ) {}

  @UseGuards(AuthGuard('jwt'))
  @Post('create-game')
  async createGame(@Body() game: GameDto): Promise<any> {
    const response = await this.createGameInteractor.execute(game);
    if (response.error) {
      throw new BadRequestException(response);
    }
    return response;
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('next-word/:gameId')
  async nextWord(@Param('gameId') gameId: string): Promise<any> {
    const response = await this.generateNextWordInteractor.execute(gameId);
    if (response.error) {
      throw new BadRequestException(response);
    }
    return response;
  }

  @UseGuards(AuthGuard('jwt'))
  @Post('attempt')
  async attempt(@Body() request: AttemptRequestDto): Promise<any> {
    const response = await this.attemptsWordInteractor.execute(request);
    if (response.error) {
      throw new BadRequestException(response);
    }
    return response;
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('most-successful-words')
  async mostSuccessfulWords(): Promise<any> {
    const response = await this.mostSuccessfulWordsInteractor.execute();
    if (response.error) {
      throw new BadRequestException(response);
    }
    return response;
  }
}
