import { BadRequestException, Body, Controller, Post } from '@nestjs/common';
import { AuthInteractor } from '../../application-core/auth/use-cases/auth.interactor';
import { LoginUserDto, UserDto } from '../../application-core/auth/dtos/index';

@Controller('auth')
export class AuthController {
  constructor(private readonly authInteractor: AuthInteractor) {}

  @Post('register')
  async register(@Body() userDto: UserDto): Promise<any> {
    const response = await this.authInteractor.register(userDto);
    if (response.error) {
      throw new BadRequestException(response);
    }
    return response;
  }

  @Post('login')
  async login(@Body() loginUserDto: LoginUserDto): Promise<any> {
    const response = await this.authInteractor.login(loginUserDto);
    if (response.error) {
      throw new BadRequestException(response);
    }
    return response;
  }
}
