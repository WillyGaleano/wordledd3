import {
  BadRequestException,
  Controller,
  Get,
  Param,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { StatisticsInteractor } from '../../application-core/user/use-cases/statistics.interactor';
import { Top10Interactor } from '../../application-core/user/use-cases/top10.interactor';

@Controller('user')
export class UserController {
  constructor(
    private readonly statisticsInteractor: StatisticsInteractor,
    private readonly top10Interactor: Top10Interactor,
  ) {}

  @UseGuards(AuthGuard('jwt'))
  @Get('statistics/:userId')
  async statistics(@Param('userId') userId: string): Promise<any> {
    const response = await this.statisticsInteractor.execute(userId);
    if (response.error) {
      throw new BadRequestException(response);
    }
    return response;
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('top10')
  async top10(): Promise<any> {
    const response = await this.top10Interactor.execute();
    if (response.error) {
      throw new BadRequestException(response);
    }
    return response;
  }
}
