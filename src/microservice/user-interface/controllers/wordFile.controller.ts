import {
  BadRequestException,
  Controller,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { GenerateWordsInteractor } from '../../application-core/wordle/use-cases/generateWords.interactor';

@Controller('wordfile')
export class WordFileController {
  constructor(
    private readonly generateWordsInteractor: GenerateWordsInteractor,
  ) {}

  @UseGuards(AuthGuard('jwt'))
  @Post('generate')
  async generateWord(): Promise<any> {
    const response = await this.generateWordsInteractor.execute();
    if (response.error) {
      throw new BadRequestException(response);
    }
    return response;
  }
}
