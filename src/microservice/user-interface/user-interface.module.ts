import { Module } from '@nestjs/common';
import { ApplicationCoreModule } from '../application-core/application-core.module';
import { AuthController } from './controllers/auth.controller';
import { UserController } from './controllers/user.controller';
import { WordFileController } from './controllers/wordFile.controller';
import { WordleController } from './controllers/wordle.controller';

@Module({
  imports: [ApplicationCoreModule],
  controllers: [
    WordleController,
    WordFileController,
    AuthController,
    UserController,
  ],
})
export class UserInterfaceModule {}
