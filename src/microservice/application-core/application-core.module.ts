import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { InfrastructureModule } from '../infrastructure/infrastructure.module';
import { JwtStrategy } from './auth/strategies/jwt.strategy';
import { AuthInteractor } from './auth/use-cases';
import {
  AttemptsRepository,
  UserRepository,
  GameRepository,
  PlayRepository,
  WordFileRepository,
} from './repositories';
import { StatisticsInteractor, Top10Interactor } from './user/use-cases';
import {
  CreateGameInteractor,
  GenerateWordsInteractor,
  GenerateNextWordInteractor,
  AttemptsWordInteractor,
  MostSuccessfulWordsInteractor,
} from './wordle/use-cases';

const services = [
  JwtStrategy,
  PassportModule,
  JwtModule,
  InfrastructureModule,
  AuthInteractor,
  GameRepository,
  PlayRepository,
  WordFileRepository,
  UserRepository,
  AttemptsRepository,
  CreateGameInteractor,
  GenerateWordsInteractor,
  GenerateNextWordInteractor,
  AttemptsWordInteractor,
  StatisticsInteractor,
  Top10Interactor,
  MostSuccessfulWordsInteractor,
];

@Module({
  imports: [
    ConfigModule,
    InfrastructureModule,
    PassportModule.register({ defaultStrategy: 'jwt' }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return {
          secret: configService.get('JWT_SECRET'),
          signOptions: {
            expiresIn: '2h',
          },
        };
      },
    }),
  ],
  exports: services,
  providers: services,
})
export class ApplicationCoreModule {}
