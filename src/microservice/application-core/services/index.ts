import { createReadStream } from 'fs';

export const readWords = (wordSize: number): Promise<string[]> => {
  return new Promise((resolve, reject) => {
    const words: string[] = [];
    const stream = createReadStream(
      'src/microservice/application-core/services/resources/words.txt',
      {
        autoClose: true,
      },
    );
    stream.on('data', (chunk) => {
      const data = chunk.toString();
      const dataSplit = data.split(/\r\n|\r|\n/);
      const filteredWords = dataSplit.filter(
        (word) => word.length === wordSize,
      );
      words.push(...filteredWords);
    });
    stream.on('end', () => {
      resolve(words);
    });
    stream.on('error', (err) => {
      console.log(`error: ${err}`);
      reject(err);
    });
  });
};
