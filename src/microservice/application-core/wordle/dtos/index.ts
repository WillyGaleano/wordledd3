import { IsNotEmpty, Length } from 'class-validator';

export class GameDto {
  @IsNotEmpty({ message: 'PlayId is required' })
  userId: string;
}

export class ResponseDto<T> {
  constructor(
    public readonly data?: T,
    public readonly message?: string,
    public readonly error?: any,
  ) {}
}

export class AttemptRequestDto {
  @IsNotEmpty({ message: 'PlayId is required' })
  playId: string;

  @IsNotEmpty({ message: 'Word is required' })
  @Length(5, 5, { message: 'Word must be at least 5 characters' })
  user_word: string;
}
