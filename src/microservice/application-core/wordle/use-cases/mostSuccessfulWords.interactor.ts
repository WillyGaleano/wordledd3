import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { PlayRepository } from '../../repositories';
import { ResponseDto } from '../../wordle/dtos/index';

@Injectable()
export class MostSuccessfulWordsInteractor {
  constructor(private playRepository: PlayRepository) {}

  async execute(): Promise<ResponseDto<any>> {
    try {
      const plays = await this.playRepository.getAllSuccess();

      const words = plays.map((play) => play.word);
      const wordsGrouped = words.reduce((acc, word) => {
        if (acc[word]) {
          acc[word] += 1;
        } else {
          acc[word] = 1;
        }
        return acc;
      }, {});

      const wordsSorted = Object.keys(wordsGrouped).sort(
        (a, b) => wordsGrouped[b] - wordsGrouped[a],
      );

      return new ResponseDto(wordsSorted);
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
