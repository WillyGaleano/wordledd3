import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import {
  GameRepository,
  PlayRepository,
  WordFileRepository,
} from '../../repositories';
import { getRandomWord } from '../../utils';
import { ResponseDto } from '../dtos/index';

@Injectable()
export class GenerateNextWordInteractor {
  constructor(
    private wordFileRepository: WordFileRepository,
    private gameRepository: GameRepository,
    private playRepository: PlayRepository,
  ) {}
  async execute(gameId: string): Promise<ResponseDto<any>> {
    try {
      const countWordFiles = await this.wordFileRepository.count();
      if (countWordFiles === 0) {
        return new ResponseDto(null, 'No words available');
      }

      const game = await this.gameRepository.getById(gameId);
      if (!game) {
        return new ResponseDto(null, 'Game not found');
      }

      const plays = await this.playRepository.getAllByGameId(gameId);
      const words = plays.map((play) => play.word);

      const wordFiles = await this.wordFileRepository.getUnselectedWords(words);
      const newWord = getRandomWord(wordFiles);
      if (!newWord) {
        return new ResponseDto(null, 'No word available');
      }

      const newPlay = await this.playRepository.create({
        gameId: game.id,
        word: newWord,
      });

      return new ResponseDto(newPlay, 'Next word generated');
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
