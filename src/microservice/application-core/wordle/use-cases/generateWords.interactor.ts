import { readWords } from '../../services';
import { WordFileRepository } from '../../repositories/wordFile.repository';
import { Prisma } from '@prisma/client';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { ResponseDto } from '../dtos/index';

@Injectable()
export class GenerateWordsInteractor {
  constructor(private wordFileRepository: WordFileRepository) {}
  async execute(): Promise<ResponseDto<any>> {
    try {
      const existsFiles = await this.wordFileRepository.count();
      if (existsFiles > 0) {
        return new ResponseDto(null, 'Words already exists');
      }
      const words = await readWords(5);
      const wordsJson = words.map((word) => {
        return { name: word } as Prisma.WordFileCreateManyInput;
      });
      const resp = await this.wordFileRepository.createBatch(wordsJson);
      return new ResponseDto(resp);
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
