import { Injectable, HttpException, HttpStatus } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { WordFileRepository } from '../../repositories';
import { GameDto, ResponseDto } from '../dtos';
import { getRandomWord } from '../../utils/index';

@Injectable()
export class CreateGameInteractor {
  constructor(private wordFileRepository: WordFileRepository) {}
  async execute(game: GameDto): Promise<ResponseDto<any>> {
    try {
      const prisma = new PrismaClient();
      const { userId } = game;

      const user = await prisma.user.findUnique({
        where: { id: userId },
      });
      if (!user) {
        return new ResponseDto(null, 'User not found');
      }

      const wordFiles = await this.wordFileRepository.getUnselectedWords([]);
      const newWord = getRandomWord(wordFiles);
      if (!newWord) {
        return new ResponseDto(null, 'No word available');
      }

      const response = await prisma.$transaction(async (tx) => {
        const newGame = await tx.game.create({
          data: {
            userId,
          },
        });

        const newPlay = await tx.play.create({
          data: {
            gameId: newGame.id,
            word: newWord,
          },
        });
        return { game: newGame, play: newPlay };
      });

      return new ResponseDto(response);
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
