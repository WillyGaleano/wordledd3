import { Injectable, HttpException } from '@nestjs/common';
import { AttemptRequestDto } from '../dtos';
import { AttemptsRepository, PlayRepository } from '../../repositories';
import { ResponseDto } from '../dtos/index';
import { HttpStatus } from '@nestjs/common/enums';

@Injectable()
export class AttemptsWordInteractor {
  constructor(
    private playRepository: PlayRepository,
    private attemptsRepository: AttemptsRepository,
  ) {}

  async execute(request: AttemptRequestDto): Promise<ResponseDto<any>> {
    try {
      const { playId, user_word } = request;
      const play = await this.playRepository.getById(playId);
      if (!play) {
        throw new Error('Play not found');
      }

      if (play.completed) {
        throw new Error('You already won');
      }

      const attemps = await this.attemptsRepository.getAllByPlayId(play.id);
      if (attemps.length >= 5) {
        throw new Error('You have reached the maximum number of attempts');
      }

      if (!play.word) {
        throw new Error('Word not found');
      }

      const wordPlayArray = play.word.split('');
      const userWordArray = user_word.split('');

      const letters = [];
      userWordArray.forEach((letter, index) => {
        if (wordPlayArray.includes(letter)) {
          if (wordPlayArray[index] === letter) {
            letters.push({ letter, value: 1 });
          } else {
            letters.push({ letter, value: 2 });
          }
        } else {
          letters.push({ letter, value: 3 });
        }
      });

      const total = letters.reduce((acc, letter) => {
        return acc + letter.value;
      }, 0);

      if (total === 5) {
        await this.playRepository.update(play.id, { completed: true });
      }
      const newAttempts = await this.attemptsRepository.create({
        playId: play.id,
        userWord: user_word,
        letters,
      });

      return new ResponseDto(newAttempts, 'Attempt created');
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
