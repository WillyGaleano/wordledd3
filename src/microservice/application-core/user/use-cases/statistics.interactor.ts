import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { GameRepository, UserRepository } from '../../repositories';
import { ResponseDto } from '../../wordle/dtos/index';

@Injectable()
export class StatisticsInteractor {
  constructor(
    private userRepository: UserRepository,
    private gameRepository: GameRepository,
  ) {}

  async execute(userId: string): Promise<ResponseDto<any>> {
    try {
      const user = await this.userRepository.getById(userId);
      if (!user) {
        throw new Error('User not found');
      }

      const games = await this.gameRepository.getAllByUserId(user.id);

      const cantPlays = games.reduce((acc, game) => {
        return acc + game.Plays.length;
      }, 0);

      const cantWins = games.reduce((acc, game) => {
        return acc + game.Plays.filter((play) => play.completed).length;
      }, 0);

      return new ResponseDto({
        userId: user.id,
        name: user.name,
        lastName: user.lastName,
        cantPlays,
        cantWins,
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
