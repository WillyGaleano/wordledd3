import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { GameRepository, UserRepository } from '../../repositories';
import { ResponseDto } from '../../wordle/dtos/index';

@Injectable()
export class Top10Interactor {
  constructor(
    private userRepository: UserRepository,
    private gameRepository: GameRepository,
  ) {}

  async execute(): Promise<ResponseDto<any>> {
    try {
      const games = await this.gameRepository.getAll();
      const users = await this.userRepository.getAll();
      const respList = [];

      users.forEach((user) => {
        const userGames = games.filter((game) => game.userId === user.id);
        const cantWins = userGames.reduce((acc, game) => {
          return acc + game.Plays.filter((play) => play.completed).length;
        }, 0);

        respList.push({
          userId: user.id,
          name: user.name,
          lastName: user.lastName,
          cantWins,
        });
      });

      return new ResponseDto(
        respList.sort((a, b) => b.cantWins - a.cantWins).slice(0, 10),
      );
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
