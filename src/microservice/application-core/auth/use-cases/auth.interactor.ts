import * as bcrypt from 'bcrypt';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserRepository } from '../../repositories';
import { ResponseDto } from '../../wordle/dtos/index';
import { LoginUserDto, UserDto } from '../dtos';
import { JwtPayload } from '../interfaces';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthInteractor {
  constructor(
    private userRepository: UserRepository,
    private readonly jwtService: JwtService,
  ) {}

  async register(userRegister: UserDto): Promise<ResponseDto<any>> {
    try {
      const { password, ...userData } = userRegister;

      const user = await this.userRepository.create({
        ...userData,
        password: bcrypt.hashSync(password, 10),
      });

      delete user.password;

      const resp = {
        ...user,
        token: this.getJwtToken({ id: user.id }),
      };

      return new ResponseDto(resp);
    } catch (error) {
      return new ResponseDto(null, error.message);
    }
  }

  async login(loginUserDto: LoginUserDto): Promise<ResponseDto<any>> {
    try {
      const { password, email } = loginUserDto;

      const user = await this.userRepository.getByEmail(email);

      if (!user)
        throw new UnauthorizedException('Credentials are not valid (email)');

      if (!bcrypt.compareSync(password, user.password))
        throw new UnauthorizedException('Credentials are not valid (password)');

      delete user.password;
      const resp = {
        ...user,
        token: this.getJwtToken({ id: user.id }),
      };

      return new ResponseDto(resp);
    } catch (error) {
      return new ResponseDto(null, error.message);
    }
  }

  private getJwtToken(payload: JwtPayload) {
    const token = this.jwtService.sign(payload, { expiresIn: '1h' });
    return token;
  }
}
