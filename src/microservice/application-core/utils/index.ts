import { WordFile } from '@prisma/client';

export const getRandomWord = (words: WordFile[]): string => {
  if (words?.length === 0) {
    return null;
  }
  const word = words[Math.floor(Math.random() * words.length)];
  return word.name;
};
