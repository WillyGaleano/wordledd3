import { Injectable } from '@nestjs/common';
import { Attempt, Prisma } from '@prisma/client';
import { PrismaService } from '../../infrastructure/persistence/prisma/prisma.service';

@Injectable()
export class AttemptsRepository {
  constructor(private prisma: PrismaService) {}

  async create(data: Prisma.AttemptUncheckedCreateInput): Promise<Attempt> {
    return this.prisma.attempt.create({
      data,
      include: { play: { select: { id: true, completed: true, word: true } } },
    });
  }

  async getAllByPlayId(playId: string): Promise<Attempt[]> {
    return this.prisma.attempt.findMany({
      where: { playId },
    });
  }
}
