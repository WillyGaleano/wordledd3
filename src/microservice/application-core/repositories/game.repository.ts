import { Injectable } from '@nestjs/common';
import { Game, Prisma } from '@prisma/client';
import { PrismaService } from '../../infrastructure/persistence/prisma/prisma.service';

@Injectable()
export class GameRepository {
  constructor(private prisma: PrismaService) {}

  async create(data: Prisma.GameUncheckedCreateInput): Promise<Game> {
    return this.prisma.game.create({
      data,
    });
  }

  async getById(id: string): Promise<Game> {
    return this.prisma.game.findUnique({
      where: { id },
      include: { user: { select: { id: true, name: true, email: true } } },
    });
  }

  async getAll(): Promise<
    (Game & {
      Plays: {
        id: string;
        gameId: string;
        word: string;
        completed: boolean;
      }[];
    })[]
  > {
    const resp = this.prisma.game.findMany({
      include: {
        Plays: {
          select: {
            id: true,
            gameId: true,
            word: true,
            completed: true,
          },
        },
      },
    });
    return resp;
  }

  async getAllByUserId(userId: string): Promise<
    (Game & {
      Plays: {
        id: string;
        gameId: string;
        word: string;
        completed: boolean;
      }[];
    })[]
  > {
    const resp = this.prisma.game.findMany({
      where: { userId },
      include: {
        Plays: {
          select: {
            id: true,
            gameId: true,
            word: true,
            completed: true,
          },
        },
      },
    });
    return resp;
  }
}
