import { Injectable } from '@nestjs/common';
import { Prisma, WordFile } from '@prisma/client';
import { PrismaService } from '../../infrastructure/persistence/prisma/prisma.service';
@Injectable()
export class WordFileRepository {
  constructor(private prisma: PrismaService) {}

  async createBatch(
    data: Prisma.WordFileCreateManyInput[],
  ): Promise<[Prisma.BatchPayload]> {
    const resp = await this.prisma.$transaction([
      this.prisma.wordFile.createMany({
        data,
      }),
    ]);

    return resp;
  }

  async count(): Promise<number> {
    const resp = await this.prisma.wordFile.count();
    return resp;
  }

  async getUnselectedWords(name: string[]): Promise<WordFile[]> {
    const resp = await this.prisma.wordFile.findMany({
      where: {
        name: {
          notIn: name,
        },
      },
    });
    return resp;
  }
}
