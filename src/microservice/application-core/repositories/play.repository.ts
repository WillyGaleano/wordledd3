import { Injectable } from '@nestjs/common';
import { Play, Prisma } from '@prisma/client';
import { PrismaService } from '../../infrastructure/persistence/prisma/prisma.service';
@Injectable()
export class PlayRepository {
  constructor(private prisma: PrismaService) {}

  async update(
    id: string,
    data: Prisma.PlayUncheckedUpdateInput,
  ): Promise<Play> {
    return this.prisma.play.update({
      where: { id },
      data,
    });
  }

  async getById(id: string): Promise<Play> {
    return this.prisma.play.findUnique({
      where: { id },
      include: { game: { select: { id: true, userId: true } } },
    });
  }

  async getAllByGameId(gameId: string): Promise<Play[]> {
    return this.prisma.play.findMany({
      where: { gameId },
    });
  }

  async getByGameId(gameId: string): Promise<Play> {
    return this.prisma.play.findFirst({
      where: { gameId },
    });
  }

  async create(data: Prisma.PlayUncheckedCreateInput): Promise<Play> {
    return this.prisma.play.create({
      data,
    });
  }

  async getAllSuccess(): Promise<Play[]> {
    return this.prisma.play.findMany({
      where: { completed: true },
    });
  }
}
