export { GameRepository } from './game.repository';
export { PlayRepository } from './play.repository';
export { WordFileRepository } from './wordFile.repository';
export { UserRepository } from './user.repository';
export { AttemptsRepository } from './attempts.repository';
